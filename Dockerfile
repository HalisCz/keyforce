FROM alpine
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
COPY build_out/keyforce /bin/

CMD ["/bin/keyforce"]
